# README #

This project is a Liferay 6.2 portlet

### What is this repository for? ###

WeLive manage employees portlet

### How do I get set up? ###

For details about configuration/installation you can see "D2.3 – WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V1"
Remember to set the username/password for the Basic Authentication on the file \docroot\WEB-INF\src\config.properties

### Who do I talk to? ###

* Tecnalia team,  Sara Sillaurren