<%@page import="javax.portlet.RenderRequest"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@page import="com.liferay.portal.model.User"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.welive.manage.utils.Utils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Enumeration"%>
<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL name="addEmploy" var="addEmployURL"></portlet:actionURL>

<portlet:actionURL name="removeEmploy" var="removeEmployURL"></portlet:actionURL>

<%
try{
	
	
	List<Organization> mylist= user.getOrganizations(); //OrganizationLocalServiceUtil.getUserOrganizations(ccUserId);
	long orgid = -1;
	String pilot="";
	Organization companyorg =null;
	for (Organization org : mylist){
		
		if (org.getExpandoBridge().getAttribute("isCompany").toString().equalsIgnoreCase("true")){
			orgid = org.getOrganizationId();
			companyorg=org;
	 		break;        
		}
	}	
 
	
	if (orgid <= 0){
		%>
		-The user :  <%=user.getFirstName()%> doesn't have an Organization! <br/>
		<%
	}
	else{
			//}
	    String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
	    if(pilotArray != null && pilotArray.length > 0) {
	        pilot = pilotArray[0];
	    }
	
	    List<User> list = new ArrayList<User>();   
	    Integer delta = ParamUtil.getInteger(request, "delta");       
	    Integer cur = ParamUtil.getInteger(request, "cur"); // (Integer)request.getAttribute("cur");  
	    String search = ParamUtil.getString(request, "search");// (String) request.getAttribute("search");
	    if(cur == null){
	        cur = 1;
	    }
	    if(delta == null){
	        delta = 20;
	    }
	    if(search == null){
	        search = "";    
	    }
	    //PortletURL portletURL = renderResponse.createActionURL();
	    
	    List<User> user_list = UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS, QueryUtil.ALL_POS);

	    // search(company.getCompanyId(), "",0, null, searchContainer.getStart(), searchContainer.getEnd(),
		//		searchContainer.getOrderByComparator());
	    if(user_list != null){
	    	for (User nuser : user_list){
	    		boolean samecity=false;	    		
	    		String auxpilot=""; 
	    	    pilotArray = (String [])nuser.getExpandoBridge().getAttribute("pilot");
	    	    if(pilotArray != null && pilotArray.length > 0) {
	    	    	auxpilot = pilotArray[0];
	    	    }		
	    		Long ccUserId = (Long)nuser.getExpandoBridge().getAttribute("CCUserID");
	    		if (!Utils.isIMSSimpleUser(nuser) || !auxpilot.equals(pilot) || user.getUserId() == nuser.getUserId()){
	    			continue;
	    		}	
	    		mylist= nuser.getOrganizations();//OrganizationLocalServiceUtil.getUserOrganizations(nuser.getUserId());
	    		boolean ismyincomapny = false;
	    		boolean isinothercomapny = false;
	    		for (Organization org : mylist){
	    			if (org.getExpandoBridge().getAttribute("isCompany").toString().equalsIgnoreCase("true")){
	    				
	    				if (orgid == org.getOrganizationId()){
	    					ismyincomapny = true;
	    				}
	    				else{
	    					isinothercomapny= true;
	    				}
	    				break;
	    					
	    				
	    			}
	    		}
	    		if (!isinothercomapny){
	    			if (search.trim().equals("")){
	    				list.add(nuser);
	    				
	    			}
	    			else{
	    				if (nuser.getEmailAddress().contains(search) ||
	    						nuser.getFirstName().contains(search) ||
	    						nuser.getFullName().contains(search) ||
	    						nuser.getLastName().contains(search) 
	    						){
	    					list.add(nuser);
	    				}
	    			}
	    		}	           	    		
	    		
	    	}
	    }
	    int count = list.size();
	    String scur = ""+cur;
	    String sdelta = ""+delta;
	   // portletURL.setParameter("cur", String.valueOf(cur));
	  %>
	<liferay-portlet:renderURL varImpl="iteratorURL">
	       <portlet:param name="mvcPath" value="/html/manageemployees/view.jsp" />
			<portlet:param name="cur" value="<%= scur %>" />
			<portlet:param name="delta" value="<%= sdelta %>" />       
	</liferay-portlet:renderURL>
	<aui:form name="fm" action="<%=iteratorURL.toString()%>" method="post">
		<aui:layout>
       		<aui:column>
			<aui:input type="text" name="search" label="Search term" inlineLabel="true"></aui:input>
			</aui:column>
			<aui:column>
			<aui:button type="submit" value="Search"></aui:button>
			</aui:column>
		</aui:layout>
	</aui:form>
	<liferay-ui:search-container iteratorURL="<%= iteratorURL %>" delta="<%= delta %>" emptyResultsMessage="No Users Found." deltaConfigurable="true">

    <liferay-ui:search-container-results  >
    <%
    
    	results = ListUtil.subList(list,searchContainer.getStart(), searchContainer.getEnd());
        total = list.size();
        pageContext.setAttribute("results", results);
        pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row modelVar="luser" className="com.liferay.portal.model.User" >
        <%   
	
        String sname = luser.getFirstName();
        String smail  = luser.getEmailAddress();
        String scompany  = "";
		mylist= luser.getOrganizations();//OrganizationLocalServiceUtil.getUserOrganizations(nuser.getUserId());
		boolean ismyincomapny = false;
		boolean isinothercomapny = false;
		for (Organization org : mylist){
			if (org.getExpandoBridge().getAttribute("isCompany").toString().equalsIgnoreCase("true")){
				scompany =org.getName();
				if (orgid == org.getOrganizationId()){
					ismyincomapny = true;
				}
				else{
					isinothercomapny= true;
				}
				break;
				
			}
			
		}
				       
        %>
        <liferay-ui:search-container-column-text name="Name">
            <%= sname %>
        </liferay-ui:search-container-column-text>
       <liferay-ui:search-container-column-text name="Mail">
            <%= smail %>
        </liferay-ui:search-container-column-text>
      <liferay-ui:search-container-column-text name="Company">
            <%= scompany %>
       </liferay-ui:search-container-column-text>
        <liferay-ui:search-container-column-text name="Action">
 		<%if (!isinothercomapny){
		 		           

			if (!ismyincomapny){
				
				%>
				<aui:form action="<%= addEmployURL %>" name="<portlet:namespace/>fm">
				<input type="hidden" id="<portlet:namespace/>userid" name="<portlet:namespace/>userid" value="<%=luser.getUserId()%>" />
				<input type="hidden" id="<portlet:namespace/>orgid" name="<portlet:namespace/>orgid" value="<%=orgid%>" />
				<aui:button-row >		
				<aui:button type="submit" value="Add Employ"></aui:button>	
				</aui:button-row>	
				</aui:form>		
				<%			
			}
			else{
				%>
				<aui:form action="<%= removeEmployURL %>" name="<portlet:namespace/>fm">
				<input type="hidden" id="<portlet:namespace/>userid" name="<portlet:namespace/>userid" value="<%=luser.getUserId()%>" />
				<input type="hidden" id="<portlet:namespace/>orgid" name="<portlet:namespace/>orgid" value="<%=orgid%>" />
				
				<aui:button-row >
				<aui:button type="submit" value="Remove Employ"></aui:button>
				</aui:button-row>	
				</aui:form>			
				<%			
				
			}
		}%>
 		 </liferay-ui:search-container-column-text>
       
    </liferay-ui:search-container-row>
    
    <liferay-ui:search-iterator searchContainer="<%=searchContainer %>" />
	</liferay-ui:search-container>

	
	<%	

  }
	
}
catch(Exception e){
	%>
	<%=e.getMessage()%>
	<%	
}
%>