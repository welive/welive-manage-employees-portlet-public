package com.welive.manage.portlet;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.welive.manage.utils.Propagate;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * Portlet implementation class ManageEmployees
 */
public class ManageEmployees extends MVCPortlet {
	private static Log _log = LogFactoryUtil
			.getLog(ManageEmployees.class);
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public ManageEmployees() {
        super();
    }
    
    public void addEmploy(ActionRequest request, ActionResponse response) {
    	long userid = ParamUtil.getLong(request, "userid");
    	long orgid = ParamUtil.getLong(request, "orgid");
    	_log.info("Propagation of the add user to organization userid:"+userid+" orgid:"+orgid);
    	if (userid==0 || orgid <= 0){
			_log.error("The propagation of the add user to organization  could not be performed userid:"+userid+" orgid:"+orgid);
			SessionErrors.add(request, "error_propagation");

    	}
    	else{
			//Organization org =OrganizationLocalServiceUtil.getOrganization(orgid);
			try {
				_log.info("UserLocalServiceUtil.addOrganizationUser with userid: "+userid+" orgid: "+orgid);
				UserLocalServiceUtil.addOrganizationUser(orgid, userid);
				_log.info("Propagate.getInstance().userAddOrganizationMembersPropagate with userid: "+userid+" orgid: "+orgid+" add:"+true);
		        String code = Propagate.getInstance().userAddOrganizationMembersPropagate(userid, orgid, true);
				if (!"OK".equals(code)) {
					_log.error("The propagation of the add user to organization  could not be performed successfully.");
					SessionErrors.add(request, "error_propagation");
				}
			} catch (SystemException e) {
				_log.error("The propagation of the add user to organization  could not be performed successfully:"+e.getMessage());
				_log.error(e);
			}
					
    	}
    }
    
    public void removeEmploy(ActionRequest request, ActionResponse response) {
    	long userid = ParamUtil.getLong(request, "userid");
    	long orgid = ParamUtil.getLong(request, "orgid");
    	_log.info("Propagation of the remove user from organization userid:"+userid+" orgid:"+orgid);
    	if (userid==0 || orgid <= 0){
			_log.error("The propagation of the remove user from organization  could not be performed userid:"+userid+" orgid:"+orgid);
			SessionErrors.add(request, "error_propagation");

    	}
    	else{
    	
	    	try {
				_log.info("UserLocalServiceUtil.deleteOrganizationUser with userid: "+userid+" orgid: "+orgid);
				UserLocalServiceUtil.deleteOrganizationUser(orgid, userid);
				_log.info("Propagate.getInstance().userAddOrganizationMembersPropagate with userid: "+userid+" orgid: "+orgid+" add:"+false);
		        String code = Propagate.getInstance().userAddOrganizationMembersPropagate(userid, orgid, false);
				if (!"OK".equals(code)) {
					_log.error("The propagation of the remove user from organization  could not be performed successfully.");
					SessionErrors.add(request, "error_propagation");
				}
			} catch (SystemException e) {
				_log.error("The propagation of the remove user from organization  could not be performed successfully: "+e.getMessage());
				_log.error(e);
			}
    	}
		
		
    }
}
