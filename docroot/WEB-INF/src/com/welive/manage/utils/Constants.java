package com.welive.manage.utils;

public final class Constants {
	public final static String YES = "yes";
	public final static String TRUE = "true";
	
	public final static String PILOT_CITY_TRENTO = "Trento";
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_HELSINKI = "Uusimaa";
	public final static String PILOT_CITY_LIST[] = {PILOT_CITY_TRENTO, PILOT_CITY_BILBAO, PILOT_CITY_NOVISAD, PILOT_CITY_HELSINKI};
	
	public final static String ROLE_CITIZEN = "Citizen";
	public final static String ROLE_ACADEMY = "Academy";
	public final static String ROLE_BUSINESS = "Business";
	public final static String ROLE_ENTREPRENEURE = "Entrepreneur";
	public final static String ROLE_DEVELOPER = "Developer";
	public final static String ROLE_COMPANY_LEADER = "CompanyLeader";
	public final static String ROLE_LIST[] = {ROLE_CITIZEN, ROLE_ACADEMY, ROLE_BUSINESS, ROLE_ENTREPRENEURE};

	public final static String EMPLOYMENT_STUDENT = "Student";
	public final static String EMPLOYMENT_UNEMPLOYED = "Unemployed";
	public final static String EMPLOYMENT_3RD = "Employedbythirdparty";
	public final static String EMPLOYMENT_SELF = "Selfemployedentrepreneur";
	public final static String EMPLOYMENT_RETIRED = "Retired";
	public final static String EMPLOYMENT_OTHER = "Other";
	public final static String EMPLOYMENT_LIST[] = {EMPLOYMENT_STUDENT, EMPLOYMENT_UNEMPLOYED, EMPLOYMENT_3RD, EMPLOYMENT_SELF, EMPLOYMENT_RETIRED, EMPLOYMENT_OTHER};
	
	public final static String LANGUAGE_ENGLISH = "English";
	public final static String LANGUAGE_SPANISH = "Spanish";
	public final static String LANGUAGE_ITALIAN = "Italian";
	public final static String LANGUAGE_FINNISH = "Finnish";
	public final static String LANGUAGE_SERBIAN = "Serbian";
	public final static String LANGUAGE_SERBIAN_LATIN = "SerbianLatin";
	public final static String LANGUAGE_LIST[] = {LANGUAGE_ENGLISH, LANGUAGE_SPANISH, LANGUAGE_ITALIAN, LANGUAGE_FINNISH, LANGUAGE_SERBIAN, LANGUAGE_SERBIAN_LATIN};

	public final static String TOOL_WC = "Controller";
	public final static String TOOL_OIA = "Open Innovation Area";
	public final static String TOOL_ODS = "Open Data Stack";
	public final static String TOOL_CDV = "Citizen Data Vault";
	public final static String TOOL_VC = "Visual Composer";
	public final static String TOOL_MKP = "Marketplace";
	public final static String TOOL_ADS = "Analytics Dashboard";
	public final static String TOOL_LIST[] = {TOOL_OIA, TOOL_ODS, TOOL_CDV, TOOL_VC, TOOL_MKP, TOOL_ADS};
	
	public final static int CODE_ACTION_SUCCESS = 200;
	public final static int CODE_ACTION_FAILED = 500;
	public final static int CODE_CCUSERID_NOT_FOUND = 404;
	public final static int CODE_INPUTS_ERROR = 400;
	public final static int CODE_RESOURCE_ERROR = 424;
	public final static int CODE_AUTHORIZATION_ERROR = 401;
	
}
