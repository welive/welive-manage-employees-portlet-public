package com.welive.manage.utils;

import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.kernel.exception.SystemException;
public class Utils {

	/**
	 * @param errorCode
	 * @return
	 */
	public static String getErrorDescriptionByCode(int errorCode){
		
		
		Map<Integer, String> mapping = new HashMap<Integer, String>();
		
		mapping.put( Constants.CODE_ACTION_SUCCESS , "ACTION_SUCCESS");
		mapping.put( Constants.CODE_ACTION_FAILED , "ACTION_FAILED");
		mapping.put( Constants.CODE_CCUSERID_NOT_FOUND , "CCUSERID_NOT_FOUND");
		mapping.put( Constants.CODE_INPUTS_ERROR , "INPUTS_ERROR");
		mapping.put( Constants.CODE_RESOURCE_ERROR , "RESOURCE_ERROR");
		mapping.put( Constants.CODE_AUTHORIZATION_ERROR , "AUTHORIZATION_ERROR");
		
		String descr = mapping.get(errorCode);
		
		if (Validator.isNull(descr))
		 descr = "other";
		
		return descr;
		
	}
public static boolean isIMSSimpleUser (User currentUser){
		
		if (Validator.isNull(currentUser) ||  !currentUser.isActive() )
			return false;
		

		boolean isAcademy=false;
		boolean isBusiness=false;
		boolean isCitizen=false;
		boolean isEntrepreneur=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(Constants.ROLE_ACADEMY)){
			  		isAcademy=true;
			  		break;
			  	}else if(role.getName().equalsIgnoreCase(Constants.ROLE_BUSINESS)){
			  		isBusiness=true;
			  		break;
			  	}else if(role.getName().equalsIgnoreCase(Constants.ROLE_CITIZEN)){
			  		isCitizen=true;
			  		break;
			  		
			  	}else if(role.getName().equalsIgnoreCase(Constants.ROLE_ENTREPRENEURE)){
			  		isEntrepreneur=true;
			  		break;
			  	}		
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		if (isAcademy || isBusiness || isCitizen || isEntrepreneur)
			return true;
		
		
		return false;
	}	

}
