package com.welive.manage.utils;

import it.eng.model.ExtModule;
import it.eng.service.ExtModuleLocalServiceUtil;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
public class Propagate {
	
	private static final int MESSAGE_TYPE_SUCCESS = 0;
	private static final int MESSAGE_TYPE_ERROR = 1;
	private static final int MESSAGE_TYPE_WARNING = 2;

	private static final int MESSAGE_REGISTRATION_SUCCEED = 0;
	private static final int MESSAGE_REGISTRATION_FAILED = 1;
	private static final int MESSAGE_USER_EXISTS = 2;	
	private static final int MESSAGE_JSON_ERROR = 3;
	private static final int MESSAGE_INVALID_SECRET = 4;
	private static final int MESSAGE_DECRYPTION_ERROR = 5;
	private static final int MESSAGE_EMAIL_NOT_SENT = 6;

	private static Propagate instance;

	private Log _log;
	private boolean isMsgCrypted;

	private String username;
	private String password;

	private Propagate() {
		_log = LogFactoryUtil.getLog(Propagate.class);
		isMsgCrypted = false;
		try {
			username = PrefsPropsUtil.getString("welive.username");
			password = PrefsPropsUtil.getString("welive.password");
		} catch (SystemException e) {
			username = PropertyGetter.getProperty("welive.username");
			password = PropertyGetter.getProperty("welive.password");
		}	
	}
	public static Propagate getInstance() {
		if(instance == null) {
			instance = new Propagate();
		}
		return instance;
	}
	/*
	 * �	getExtModuleByAddOrganizationMembersPropagation(true) --> user association module list
�	module.getAddOrganizationMembersEndpoint --> user association endpoint
�	getExtModuleByRemoveOrganizationMembersPropagation(true) --> user remove module list
�	module.setRemoveOrganizationMembersEndpoint--> user remove endpoint

	 */
	public String userAddOrganizationMembersPropagate(long userId,long companyId, boolean addUserToCompany ) {
		
		Map<String, String> remoteToolsInError = new HashMap<String, String>();
		String screenName = "";
		User user = null;
		try {
			user = UserLocalServiceUtil.getUser(userId);
			screenName = user.getScreenName();
		} catch (Exception e) {
			_log.error(e);
			
		}
		/*
		 *  "ccOrganizationId": <integer>*,
 			"members": [ <integer (ccUserId)> ]*

		 */

		JSONObject json = JSONFactoryUtil.createJSONObject();
		JSONArray jsonusers = JSONFactoryUtil.createJSONArray();
		long CCUserID =  Long.parseLong(user.getExpandoBridge().getAttribute("CCUserID").toString());    
		jsonusers.put(CCUserID);
		json.put("ccOrganizationId", companyId);
		json.put("members", jsonusers);
		List<ExtModule> extModuleList = new ArrayList<ExtModule>();
		try {
			if (addUserToCompany){
				extModuleList = ExtModuleLocalServiceUtil.getExtModuleByAddOrganizationMembersPropagation(true);
			}
			else{
				extModuleList = ExtModuleLocalServiceUtil.getExtModuleByRemoveOrganizationMembersPropagation(true);
			}
//			extModuleList = ExtModuleLocalServiceUtil.getExtModules(0, ExtModuleLocalServiceUtil.getExtModulesCount());
		} catch (Exception e) {
			_log.error(e);
		}

		if(extModuleList.size() > 0) {
			_log.info("Propagate AddOrganizationMembers to external modules...");
		}

		try {
			Client client = Client.create();
			// Basic Authentication
			client.addFilter(new HTTPBasicAuthFilter(username, password));
			
			WebResource webResource;
			ClientResponse clientResponse;
			JSONObject response;

			String url = "";
			String output = "";
			String message = "";
			String type = "";

			int t, m;

			for(int i = 0; i < extModuleList.size(); i++) {
				try {
					_log.info("..." + extModuleList.get(i).getModuleName());
					if (addUserToCompany){
						url = extModuleList.get(i).getAddOrganizationMembersEndpoint();
					}
					else{
						url = extModuleList.get(i).getRemoveOrganizationMembersEndpoint();
					}
					String contentType = extModuleList.get(i).getContentType();
					json.put("secret", extModuleList.get(i).getModuleSharedSecret());

					byte[] cipher = null;
					if(isMsgCrypted) {
						// Get public key of external module
						PublicKey pk = SecurityUtils.getPublicKeyByAlias(extModuleList.get(i).getModuleAlias());
						if(pk == null) {
							_log.error("Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							return "KO";
						}

						// Encrypt JSON
						cipher = SecurityUtils.encrypt(json.toString(), pk);
						if(cipher == null) {
							_log.error("Encryption error!");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Encryption error!");
							return "KO";
						}	
					}

					webResource = client.resource(url);
					_log.info("Sending message to " + url);
					if(isMsgCrypted) {
						_log.info("Encrypted message");
						clientResponse = webResource.accept("application/json").type(javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class, cipher);
					} else {
						_log.info("message: " + json.toString());
						clientResponse = webResource.accept("application/json").type(contentType).post(ClientResponse.class, json.toString());
					}

					if (clientResponse.getStatus() != 200) {
						_log.error("Failed - HTTP Error Code :" + clientResponse.getStatus());
						remoteToolsInError.put(extModuleList.get(i).getModuleName(), Utils.getErrorDescriptionByCode(clientResponse.getStatus()));
					}

					output = clientResponse.getEntity(String.class);

					if(output.substring(0, 1).compareTo("\"") == 0 && output.substring(output.length() - 1, output.length()).compareTo("\"") == 0) {
						output = output.substring(1, output.length() - 1);
					}

					output = output.replace("\\", "");

					response = JSONFactoryUtil.createJSONObject(output);
					_log.info("Response: " + response.toString());

					t = response.getInt("type");
					m = response.getInt("message");

					switch(t) {
						case MESSAGE_TYPE_SUCCESS:
							type = "SUCCESS"; break;
						case MESSAGE_TYPE_ERROR:
							type = "ERROR"; break;
						case MESSAGE_TYPE_WARNING:
							type = "WARNING"; break;
						default:
							type = "UNKNOWN";
					}

					switch(m) {
						case MESSAGE_REGISTRATION_SUCCEED:
							message = "Registration succeed"; break;
						case MESSAGE_REGISTRATION_FAILED:
							message = "Registration failed"; break;
						case MESSAGE_USER_EXISTS:
							message = "User already exists"; break;
						case MESSAGE_EMAIL_NOT_SENT:
							message = "Registration email not sent"; break;
						case MESSAGE_JSON_ERROR:
							message = "External module error in parsing JSON"; break;
						case MESSAGE_INVALID_SECRET:
							message = "Invalid secret"; break;
						case MESSAGE_DECRYPTION_ERROR:
							message = "External module error in decrypting request"; break;
						default:
							message = "Unknown";
					}

					_log.info(type + ": " + message);
				} catch(Exception e) {
    				String errorSending = "Error sending data to "+url+". "+e.getMessage();
    				_log.error(errorSending);
    				remoteToolsInError.put(extModuleList.get(i).getModuleName(), errorSending);
				}
			}
		} catch(Exception e) {
			_log.error(e);
		}

		return "OK";
	}
	
}
